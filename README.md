# RE - Securedocs

**This repo is task force by RE**

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project or you can use branch dist
$ npm run generate

```
## General rules

 Account info :
- For administrator : admin@re.com with pass 1234
- For employee : emp@re.com with pass 1234

1. Default user by database seeder (from be) use account info
2. Each both administrator and employee account type can upload documents, but employee type account was limit only see own  documents, but administrator type can get all documents from every uploaded documents
3. Notification for new uploded employee document and automaticaly update of document list is only for administrator account type
4. Any account can search document by content (by using elasticsearch) but limit in account type (see rule number 2)
5. Any account can comments the document list

## Copyright :
- [Vue Argon Design System](https://github.com/creativetimofficial/vue-argon-design-system)
- [NuxtDropzone](https://github.com/Etheryte/nuxt-dropzone)
