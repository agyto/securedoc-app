const pkg = require('./package')

module.exports = {
  mode: 'spa', // mode univerversal ga jalan

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'X-UA-Compatible', content: 'IE=edge' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1.0, maximum-scale=1.5, user-scalable=1, shrink-to-fit=no' },
      { hid: 'description', name: 'description', content: pkg.description },
      { name: 'author', content: 'Creative Tim, Cristi Jora' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' }
    ]
  },

  /*
  ** Configuration for @nuxtjs/pwa
  ** https://developer.mozilla.org/en-US/docs/Web/Manifest
  */
  manifest: {
    name: 'Secure Docs RE',
    short_name: 'SCRE',
    description: 'Securing your docs with secure docs',
    theme_color: '#172b4d',
  },

  meta: {
    mobileAppIOS: true,
    appleStatusBarStyle: '#172b4d'
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    '~assets/argon/vendor/nucleo/css/nucleo.css',
    '@fortawesome/fontawesome-free/css/all.css',
    '~assets/argon/scss/argon.scss',
    'bootstrap-vue/dist/bootstrap-vue.css',
    '~assets/transitions.css'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/argon/argon-kit',
    // '~plugins/pusher.js'
    { src: '~plugins/pusher.js', ssr: false },
  ],

  resourceHints: false,

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://bootstrap-vue.js.org/docs/
    ['bootstrap-vue/nuxt', {
      bootstrapCSS: false,
      bootstrapVueCSS: false
    }],
    ['nuxt-fontawesome', {
      component: 'fa',
      imports: [
        // import whole set
        {
          set: '@fortawesome/free-solid-svg-icons',
          icons: ['fas']
        },
        {
          set: '@fortawesome/free-brands-svg-icons',
          icons: ['fab']
        }
      ]
    }],
    // ['vue-pusher', {
    //   api_key: 'f8d7f207482810e0e98e',
    //   options: {
    //       cluster: 'ap1',
    //       encrypted: true,
    //   }
    // }],
    '@nuxtjs/pwa',
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    '@nuxtjs/toast'
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    baseURL: "http://localhost:8000/api",
    proxy: false
  },
  store: true,
  /*
  ** Proxy and cors setting
  */
  proxy: {
    '/api/': { changeOrigin: true }
  },
  /*
  ** Auth
  */
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            url: '/login',
            method: 'post',
            propertyName: 'result.token'
          },
          // user: false,
          user: { 
            url: '/getuser', 
            method: 'post', 
            propertyName: false // set to local
          },
          logout: {
            url: '/logout', 
            method: 'get', 
          }
        },
        tokenRequired: true,
        tokenType: 'Bearer',
        globalToken: true,
        // autoFetchUser: true
      }
    },
    refreshToken: {
      property: '_refresh_token',
      data: '_refresh_token',
      maxAge: 60 * 60 * 24
    },
    redirect: {
      login: '/?auth=false',
      logout: '/?auth=logout',
      home: '/'
    },
    token: {
      name: 'token'
    },
    cookie: {
      // name: 'token',
      options: {
        secure: false,
      },
    }
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {

    },
    // rules: {
    //   "vue/html-self-closing": "off" // Fix v-for/template/key bug
    // },
    transpile: [
      ({ isLegacy }) => isLegacy && 'ky'
    ],
    // extractCSS: false,
    // optimization: {
    //   splitChunks: {
    //     chunks: 'async',
    //   }
    // },
    // splitChunks: {
    //   pages: false,
    //   vendor: false,
    //   commons: false,
    //   runtime: false,
    //   layouts: false
    // }
  }
}
