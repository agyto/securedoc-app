import Pusher from 'pusher-js'

export default ({ store }) => {
  Pusher.logToConsole = false;

  const socket = new Pusher('f8d7f207482810e0e98e', {
    cluster: 'ap1'
  })

  const channel = socket.subscribe('my-channel')
  channel.bind('my-event', function (data) {
    store.commit('setNotif', data)
  })
}