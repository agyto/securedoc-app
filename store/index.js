import Vue from 'vue'
import Vuex from 'vuex'

import documents from './documents/documents'

Vue.use(Vuex)

const store = () => {
  return new Vuex.Store({
    modules: {
      documents
    },
    state: {
      notif: [],
      headerSearchText: 'ss'
    },
    mutations: {
      setNotif(state, data) {
        state.notif.push(data)
      },
      setheaderSearchText(state, data) {
        state.setheaderSearchText = data
        // state.setheaderSearchText = data
        console.log("State : "+state.setheaderSearchText)
      },
    },
    getters: {
      getNotif(state) {
        return state.notif;
      },
      getSearchText(state) {
        return state.headerSearchText;
      },
      isAuthenticated(state){
        return state.auth.loggedIn
      },
      loggedUser(state){
        return state.auth.user
      }
    },
    // actions: {
    //   ACTION_SETSEARCH({ commit }, data) {
    //     return new Promise((resolve, reject) => {
    //       commit('setheaderSearchText', data)
    //       resolve(true)
    //     })
    //   },
    // }
  })
}
export default store