export const state = () => ({
  listDocuments: []
})

export default {
    namespaced: true,
    mutations: {
      set(state, data) {
        state.listDocuments = data
      },
      ADD(state, data) {
        // state.listDocuments = []
        state.listDocuments.unshift(data)
      },
      DELETE(state, id) {
        const index = state.listDocuments.findIndex((item) => item.id === id)
        state.listDocuments.splice(index, 1)
      },
      UPDATE(state, data) {
        state.listDocuments = state.listDocuments.map((item) => {
          if (item.id === data.id) {
            return Object.assign({}, item, data)
          }
          return item
        })
      }
    },
    actions: {
      async get({ commit }) {
        const token = this.$auth.$storage._state['_token.local']
        this.$axios.setHeader('Content-Type', 'application/json')
        this.$axios.setHeader('Authorization', token)
        await this.$axios.get(`/documents/getall`).then((res) => {
          // if(res.result){
            commit('set', res.data.result.data)
          // }
        })
      },
      ACTION_ADD({ commit }, data) {
        return new Promise((resolve, reject) => {
          commit('ADD', data)
          resolve(true)
        })
      },
      ACTION_DELETE({ commit }, data) {
        return new Promise((resolve, reject) => {
          commit('DELETE', data)
          resolve()
        })
      },
      ACTION_UPDATE({ commit }, data) {
        return new Promise((resolve, reject) => {
          commit('UPDATE', data)
          resolve()
        })
      }
    },
    getters: {
      GET_LIST(state) {
        return state.listDocuments
      }
    }
  }
  